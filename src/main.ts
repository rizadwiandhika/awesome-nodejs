import express from "express";

const app = express();

app.get("/", (req: express.Request, res: express.Response) => {
  res.json(["Hello World!", "mantap", "keren"]);
});
app.get("/:echo", (req: express.Request, res: express.Response) => {
  res.json({ echo: req.params.echo });
});

app.listen(3000, () => {
  console.log("Example app listening on port 3000!");
});
